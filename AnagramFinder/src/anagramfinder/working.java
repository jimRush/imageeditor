/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package anagramfinder;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import javax.swing.*;
import java.util.ArrayList;
import java.lang.String;

/**
 *
 * @author jimrush
 */
public class working {

    private static HashMap<String, ArrayList<String>> dictionary =
            new HashMap<String, ArrayList<String>>(); //dictionary

    public working() {
        readDict();
    }

    public static String sortWord(String word) {
        char[] ar = word.toCharArray();//converts to array
        Arrays.sort(ar);
        String sorted = String.valueOf(ar); //returns sorted word
        return sorted;
    }
    
    
    public void readDict() { //reads the dictionary from a file into a hashmap & assigns key

        try {
            File file = new File("dictionary.txt");
            System.out.println("File Loaded");
            Scanner scan = new Scanner(file);

            while (scan.hasNext()) { //scanner, reads in dictionary & sorts
                String word = scan.next();
                String sorted = sortWord(word);

                if (dictionary.containsKey(sorted)) { //if key already present
                    dictionary.get(sorted).add(word);//returns arraylist from key and adds word

                } else {
                    ArrayList<String> words = new ArrayList<>();
                    words.add(word);
                    dictionary.put(sorted, words);
                    //check for key
                    //if key not there, add & make new arraylist(string)
                    //if key there, add to arraylist
                }
            }
            System.out.println("Dictionary read");
        } catch (FileNotFoundException e) {
        }

    }

    /* The GUI passes a string into this method, 
     * 
     * 
    */
    
    public static void returnWord(String word) {

        //ArrayList<String> result;//arraylist of results
        ArrayList<String> result = dictionary.get(sortWord((word)));
        StringBuilder toPrint = new StringBuilder("Anagrams for: " + word + "\n\n");
            for (String s : result) {
                toPrint.append(s + '\n');
            }

        if (toPrint == null) {
            JOptionPane.showMessageDialog(null, "Ahhh, no anagrams found",
                    "oops", JOptionPane.ERROR_MESSAGE);
            
        } else {
            JOptionPane.showMessageDialog(null, toPrint.toString(),
                    "Results", JOptionPane.PLAIN_MESSAGE);
            //toPrint = new StringBuilder();

        }
    }
}
