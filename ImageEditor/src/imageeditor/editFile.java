
package imageeditor;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import javax.imageio.ImageIO;
import java.io.IOException;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.lang.Math;
import java.util.Random;
/**
 
 * @author jimrush
*This is the class where all the working takes place...
*
*/




public class editFile {
   
    
    int [][] pic; //array of integers, representing an image
    BufferedImage image;
    
    
    //setter
    public void setImage(int [][] pic) {
        this.pic=pic;
    
}

    //getter
    public int[][] getImage(){
        return pic;
}
    
/* This method loads the file into a 2D array*/
    public void loadImage (File file) {
        int [][] temp = null;
        BufferedImage img = null;
        if (file == null) {
            return;
        }
        try {
            img = ImageIO.read(file);
            System.out.printf("Loaded " + img.getHeight() + " X " + img.getWidth());
            temp = new int [img.getHeight()][img.getWidth()];
        } catch(IOException e) {System.out.printf("Image reading failed" + e);}
       Color c = new Color (img.getRGB(50,50), true);
       System.out.println(c);
       System.out.println(temp[50][50]);
       System.out.println("Image loaded");
       this.setImage(temp); //updates field
       
        
    } 
    
        public void shaveImage(){
        Color myWhite = new Color(255, 255, 255); 
        int rgb = myWhite.getRGB();
        System.out.println(rgb);
            
           for (int row = 50; row < 100; row ++) { //pic.length
            for (int col = 0; col < 100; col++) { //this.pic[0].length;
                pic[row][col] = rgb;
                double d = Math.random() * 5;
                int i = (int)(d);
                col = col + i;
                //col = col + Math.random() *5;
                //col++;
            }
        }
        
    }
    
        
   /* is passed a 2D image array of RGB and returns a bufferedImage */     
   public BufferedImage convertToImage (int[][] pic){
    
        BufferedImage bufferedImage = new BufferedImage(pic[0].length, pic.length, BufferedImage.TYPE_INT_RGB);
        for(int y=0; y < pic.length; y++){
            for(int x=0; x <pic[0].length; x++){
                bufferedImage.setRGB(x,y,pic[y][x]);  
            }
        }
        return bufferedImage; 
    
       }
    //this is the vhs algorithm 
    
      public void pauseImage() {
    	
    	Random rand = new Random();
    	int rando = 0;
    	double increment = 0; // how much each pixel gets incremented by that row
    	double amount = 1; // how much increment gets incremented by for each row
    	boolean goUp=true;
    	
        for (int row = 0; row <pic.length; row++) { //= row + (int)(Math.random() * 5)
        	if(increment <= 0){ // only make a new random number once the last triangle is done
        		rando = rand.nextInt(20); //random int generated, this is the point of the triangle
        		amount = rand.nextInt(10); // -------- CHANGE THESE TWO RANDOM NUMBER MAXES FOR CONTROL
        		goUp = true;
        	}
        	if(increment >= rando) goUp = false;
        	if(goUp==true) increment += amount;
        	else increment -= amount;        	

            for (int col = 0; col+increment < this.pic[0].length && col+increment > -1; col++) {//iterate horizontal
            	pic[row][col] = pic[row][(int)(col+increment)];
            }
   
        }
        
        
      }

          
    //this returns an image from a 2d array
    
    //this redraws the image to the jPanel
    
    //this saves the file



}